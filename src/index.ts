import {
  Route,
  UrlMatchResult,
  UrlSegment,
  UrlSegmentGroup,
} from '@angular/router';
import { Key, Path, pathToRegexp } from 'path-to-regexp';

export function customUrlMatcher(
  segments: UrlSegment[],
  group: UrlSegmentGroup,
  route: Route
): UrlMatchResult | null {
  const keys: Key[] = [];
  const re = pathToRegexp(route.path as Path, keys);

  const m = re.exec(segments.join('/'));
  if (!m) {
    return null;
  }

  const params = keys.reduce((a, key, index) => {
    const param = decodeParam(m[index + 1]);

    return {
      ...a,
      [key.name]: new UrlSegment(param, {}),
    };
  }, {});

  return {
    consumed: segments,
    posParams: params,
  };
}

function decodeParam(param: string): string {
  try {
    return decodeURIComponent(param);
  } catch (_) {
    throw new Error(`Failed to decode param "${param}"`);
  }
}
