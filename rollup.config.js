import resolve from '@rollup/plugin-node-resolve';
import del from 'rollup-plugin-delete';
import typescript from 'rollup-plugin-typescript2';
// import { terser } from 'rollup-plugin-terser';
import pkg from './package.json';

export default {
  input: 'src/index.ts',
  output: [
    {
      file: pkg.module,
      format: 'esm',
      esModule: true,
      sourcemap: true,
    },
    {
      file: pkg.main,
      format: 'cjs',
      sourcemap: true,
    },
  ],
  watch: {
    clearScreen: true,
  },
  external: (dependency) => {
    if (dependency.startsWith('.') || dependency.startsWith('/')) {
      return false;
    }
    return true;
  },
  plugins: [
    del({ targets: `dist/*` }),
    resolve(),
    typescript(),
    // terser(),
  ],
};
