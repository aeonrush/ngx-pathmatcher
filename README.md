# Angular path to regex UrlMatcher
Very small library that makes working with path in Angular Router a little easier to work with

### Install
```
npm i @aeonrush/ngx-pathmatcher
```

### How to use
```ts
import { customUrlMatcher } from '@aeonrush/ngx-pathmatcher';

const routes: Routes = [
  {
    path: '@:username',
    matcher: customUrlMatcher,
    component: UserComponent
  }
];

```